const express = require('express');
const app = express();
const path = require('path');
const exphbs = require('express-handlebars');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const upload = require('express-fileupload');
const session = require('express-session');
const flash = require('connect-flash');
const {mongoDbUrl} = require('./config/database');
const passport = require('passport');

mongoose.connect(mongoDbUrl, {useNewUrlParser: true, useCreateIndex: true, useFindAndModify: false})
	.then(db =>{
		console.log('Mongo Connected');
	}).catch(error => {
	console.log('Mongo Connection ' + error);
});

// Set View engine
const {select, generateTime, paginate} = require('./helpers/handlebars-helpers');

app.use(express.static(path.join(__dirname, 'public')));
app.engine('handlebars', exphbs({defaultLayout: 'home', helpers: {select: select, generateTime: generateTime, paginate: paginate}}));
app.set('view engine', 'handlebars');

// Upload Middleware
app.use(upload());

// Body Parsser
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

// Method Override
app.use(methodOverride('_method'));

app.use(session({
	secret: 'tokenkey',
	resave: true,
	saveUninitialized: true
}));

app.use(flash());

// Passport
app.use(passport.initialize());
app.use(passport.session());

// Local variables using middleware
app.use((req, res, next)=>{
	res.locals.user = req.user || null;
	res.locals.success_message = req.flash('success_message');
	res.locals.error_message = req.flash('error_message');
	res.locals.error = req.flash('error');
	next();
})

// Load routes
const main = require('./routes/home/main');
const admin = require('./routes/admin');
const posts = require('./routes/admin/posts');
const categories = require('./routes/admin/categories');
const comments = require('./routes/admin/comments');

// use routes
app.use('/', main);
app.use('/admin', admin);
app.use('/admin/posts', posts);
app.use('/admin/categories', categories);
app.use('/admin/comments', comments);


app.listen(4500, ()=>{
	console.log(`listening on port 4500`);
});