const express = require('express');
const router = express.Router();
const Post = require('../../models/Post');
const Category = require('../../models/Category');
const {isEmpty} = require('../../helpers/upload-helpers');
const {userAuthenticated} = require('../../helpers/authentication');

router.all('/*', userAuthenticated, (req, res, next)=>{
	req.app.locals.layout = 'admin';
	next();
});

router.get('/', (req, res)=>{
	Post.find({}).populate('category')
	.then(posts=>{
		res.render('admin/posts', {posts: posts});

	}).catch(error=>{
		console.log('Fetch post error ' + error);
	});
	
});

router.get('/my-posts', (req, res)=>{
	Post.find({user: req.user.id}).populate('category')
	.then(posts=>{
		res.render('admin/posts/my-posts', {posts: posts});

	}).catch(error=>{
		console.log('Fetch post error ' + error);
	});
})

router.get('/create', (req, res)=>{
	Category.find({}).then(categories=>{
		res.render('admin/posts/create', {categories: categories});
	});
	
});

router.post('/create', (req, res)=>{
	let errors = [];
	if(!req.body.body){
		errors.push({message: 'please a content to the body'});
	}

	if(errors.length > 0){
		res.render('admin/posts/create', {
			errors
		});
	}else{

		let filename = 'wordpress-3288417_1280.png';
		if(!isEmpty(req.files)){
			let file = req.files.file;
			filename = Date.now() + '-' + file.name;
			file.mv('./public/uploads/' + filename, (err)=>{
				if(err) throw err;
			});
			console.log('is not empty');
		}else{
			console.log('its empty');
		}
		

		let allowComments = true;
		if(req.body.allowComments){
			allowComments = true;
		}else{
			allowComments = false;
		}

		const newPost = new Post({
			user: req.user.id,
			title: req.body.title,
			status: req.body.status,
			allowComments: allowComments,
			body: req.body.body,
			category: req.body.category,
			file: filename 
		});

		newPost.save().then(savedPost => {
			req.flash('success_message', `Post ${savedPost.title} was created successfully`);
			res.redirect('/admin/posts')
		}).catch(error=>{
			console.log(error);
		});
	}
});

router.get('/edit/:id', (req, res)=>{
	// res.send(req.params.id);
	Post.findOne({_id: req.params.id}).then(post=>{
		Category.find({}).then(categories=>{
			res.render('admin/posts/edit', {post: post, categories: categories});
		});

	});

	
});

router.put('/edit/:id', (req, res)=>{
	Post.findOne({_id: req.params.id})
		.then(post=>{
			let allowComments = true;
			allowComments = req.body.allowComments ? 'true' : 'false' ;

			post.user = req.user.id;
			post.title = req.body.title;
			post.status = req.body.status;
			post.allowComments = allowComments;
			post.body = req.body.body;
			post.category = req.body.category;

			if(!isEmpty(req.files)){
				let file = req.files.file;
				filename = Date.now() + '-' + file.name;
				post.file = filename;
				file.mv('./public/uploads/' + filename, (err)=>{
					if(err) throw err;
				});
			}

			post.save().then(updatedPost=>{
				req.flash('success_message', `Post ${post.title} was updated successfully`);
				res.redirect('/admin/posts/my-posts');
			});

		});
	
});

router.delete('/:id', (req, res)=>{
	Post.findOne({_id: req.params.id}).populate('comments')
		.then(post=>{
			if(!post.comments.length < 1){
				post.comments.forEach(comment=>{
					comment.remove();
				});
			}
			post.remove().then(postRemoved=>{
				req.flash('success_message', `Post was deleted successfully`);
				res.redirect('/admin/posts/my-posts');
			});
			
		});
});



module.exports = router;