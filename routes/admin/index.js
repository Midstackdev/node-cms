const express = require('express');
const router = express.Router();
const faker = require('faker');
const Post = require('../../models/Post');
const Category = require('../../models/Category');
const Comment = require('../../models/Comment');
// const {userAuthenticated} = require('../../helpers/authentication');


router.all('/*', (req, res, next)=>{
	req.app.locals.layout = 'admin';
	next();
});

router.get('/', (req, res)=>{

	const promises = [
		Post.countDocuments({}).exec(),
		Category.countDocuments({}).exec(),
		Comment.countDocuments({}).exec()
	];

	Promise.all(promises).then(([postCount, categoryCount, commentCount])=>{
		res.render('admin/index', {postCount: postCount, categoryCount: categoryCount, commentCount: commentCount});
	});
	

	// Post.countDocuments({}).then(postCount=>{
	// 	res.render('admin/index', {postCount: postCount});
	// });
	
});

router.post('/generate-fake-posts', (req, res)=>{
	for(let i = 0; i < req.body.amount; i++){
		let post = new Post();
		post.title = faker.lorem.words();
		post.slug = faker.lorem.words();
		post.status = 'public';
		post.allowComments = faker.random.boolean();
		post.body = faker.lorem.sentences();

		post.save().then(savedPost=>{
			// console.log('All post saved')
		});
		
	}
	res.redirect('/admin');
});



module.exports = router;