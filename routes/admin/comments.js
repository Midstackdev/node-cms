const express = require('express');
const router = express.Router();
const Post = require('../../models/Post');
const Comment = require('../../models/Comment');



router.all('/*', (req, res, next)=>{
	req.app.locals.layout = 'admin';
	next();
});

router.get('/', (req, res)=>{
	Comment.find({user: req.user.id}).populate('user')
		.then(comments=>{
		res.render('admin/comments', {comments: comments});
	});
	
});

router.delete('/delete/:id', (req, res)=>{
	// console.log(req.params.id);
	Comment.deleteOne({_id: req.params.id}).then(deletedItem=>{
		Post.findOneAndUpdate({comments: req.params.id}, {$pull: {comments: req.params.id}}, (err, data)=>{
			if(err) console.log(err);
			res.redirect('/admin/comments');
		});
		
	});
});

router.post('/', (req, res)=>{
	Post.findOne({_id: req.body.id}).then(post=>{
		// console.log(post);
		const newComment = new Comment({
			user: req.user.id,
			body: req.body.body
		});
		post.comments.push(newComment);
		post.save().then(savedPost=>{
			newComment.save().then(savedComment=>{
				req.flash('success_message', 'Your comment will be revieved and posted');
				res.redirect(`/post/${post.slug}`);
			});
		});
	});
	
});

router.post('/approve-comment', (req, res)=>{
	// console.log(req.body.approvedComment);
	Comment.findByIdAndUpdate(req.body.id, {$set: {approveComment: req.body.approvedComment}}, (err, result)=>{
		if(err) return err;
		res.send(result);
	})
});









module.exports = router;